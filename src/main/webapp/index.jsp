<%-- 
    Document   : index
    Created on : 11-04-2021, 21:15:19
    Author     : stark
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
            <title>Hello, world!</title>
        <style>
        body { 
        color: #734E40;
        background-color:#F27B50 }
        
        #divPrincipal{
                background-color: #F2AA6B;
                margin-top: 30px;
                padding: 30px;
                border-radius: 10px;
        
        
        
        </style> 
  </head><br>
        <body>
            <div class="container">
                <div class="col-md-6 mx-auto text-center">
                    <div class="header-title">
                        <h4 class="wv-heading--title">
                                < EVA01 >
                        </h4>
                        <h5 class="wv-heading--subtitle">
                                By: CarlosMatta
                        </h5><br><br><br><br>
                    </div>
                </div>
             <div class="row">
             <div class="col-md-4 mx-auto" ID="divPrincipal">
            <div class="myform form ">
               <form action="" method="post" name="login">
                  <div class="form-group">
                     <input type="number" name="number"  class="form-control my-input" id="Capital" placeholder="Ingrese su capital">
                  </div>
                  <div class="form-group">
                     <input type="number" name="number"  class="form-control my-input" id="TasaAnual" placeholder="Ingrese su tasa anual">
                  </div>
                  <div class="form-group">
                     <input type="number"  name="number" id="phone"  class="form-control my-input" placeholder="Ingrese cantidad de años">
                  </div>
                 <hr>
                 <button type="submit" name="calcular" value="miBoton" class="btn btn-primary btn-block">Enviar Calculo</button>
                </form>
            </div>
         </div>
      </div>
   </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>